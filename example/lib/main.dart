import 'package:barcode_scanner/barcode_scanner_widget.dart';
import 'package:barcode_scanner/data/entities/product.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: BarcodeScannerWidget((Product product, Exception e) {
          if (product != null) {
            print('product ${product.name}');
          }
          if (e != null) {
            print('exception $e');
          }
        }),
      ),
    );
  }
}
