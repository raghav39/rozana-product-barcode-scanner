import 'package:barcode_scanner/data/entities/product.dart';
import 'package:barcode_scanner/data/remote/general_product_api_service.dart';
import 'package:barcode_scanner/utils/app_constants.dart';
import 'package:http/io_client.dart';
import 'package:jaguar_retrofit/jaguar_retrofit.dart';
import 'package:jaguar_serializer/jaguar_serializer.dart';

class ApiCaller {
  static final ApiCaller _instance = new ApiCaller._internal();
  var _repo;
  var _generalProductApiService;

  factory ApiCaller() {
    return _instance;
  }

  ApiCaller._internal() {
    this._repo = JsonRepo()..add(ProductSerializer());
  }

  set apiToken(String token) {
    this._generalProductApiService = GeneralProductApiService(
        route(AppConstants.PRODUCT_REPO_BASE_URL)
            .withClient(IOClient())
            .header('Authorization', 'Bearer $token'))
      ..jsonConverter = _repo;
  }

  GeneralProductApiService getGeneralProductApiService() {
    return this._generalProductApiService;
  }
}
