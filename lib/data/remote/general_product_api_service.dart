import 'package:barcode_scanner/data/entities/product.dart';
import 'package:jaguar_resty/jaguar_resty.dart' as resty;
import 'package:jaguar_retrofit/jaguar_retrofit.dart';

part 'general_product_api_service.jretro.dart';

@GenApiClient(path: "product")
class GeneralProductApiService extends ApiClient
    with _$GeneralProductApiServiceClient {
  final resty.Route base;

  GeneralProductApiService(this.base);

  @GetReq(path: ":barcode")
  Future<Product> getGeneralProductByBarcode(@PathParam() String barcode);
}
