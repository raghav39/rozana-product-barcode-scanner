// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'general_product_api_service.dart';

// **************************************************************************
// JaguarHttpGenerator
// **************************************************************************

abstract class _$GeneralProductApiServiceClient implements ApiClient {
  final String basePath = "product";
  Future<Product> getGeneralProductByBarcode(String barcode) async {
    var req =
        base.get.path(basePath).path(":barcode").pathParams("barcode", barcode);
    return req.go(throwOnErr: true).map(decodeOne);
  }
}
