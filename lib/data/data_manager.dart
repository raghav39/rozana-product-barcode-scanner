import 'package:barcode_scanner/data/entities/product.dart';
import 'package:barcode_scanner/data/remote/api_caller.dart';

class DataManager{
  static final DataManager _instance = DataManager._internal();
  final ApiCaller apiCaller;

  factory DataManager(String apiToken) {
    _instance.apiCaller.apiToken = apiToken;
    return _instance;
  }

  DataManager._internal(): apiCaller = ApiCaller();

  Future<Product> getGeneralProductByBarcode(String barcode) {
    return this.apiCaller.getGeneralProductApiService().getGeneralProductByBarcode(barcode);
  }

}
