import 'package:jaguar_serializer/jaguar_serializer.dart';

part 'product.jser.dart';

class Product {
  String comments;
  String name;
  String brandName;
  String unit;
  dynamic price;
  String imageUrl;
  int taxStrategyID;
  String category;
  dynamic discountedPrice;
  int pkProdID;
  String barcode;
  String csvCategory;
  int serialNumber;

  Product(
      {this.comments,
      this.name,
      this.brandName,
      this.unit,
      this.price,
      this.imageUrl,
      this.taxStrategyID,
      this.category,
      this.discountedPrice,
      this.pkProdID,
      this.barcode,
      this.csvCategory,
      this.serialNumber
      });

  Map<String, dynamic> toJson() => serializer.toMap(this);

  static final serializer = ProductSerializer();

  static Product fromMap(Map map) => serializer.fromMap(map);

  String toString() => toJson().toString();
}

@GenSerializer()
class ProductSerializer extends Serializer<Product> with _$ProductSerializer {}
