// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product.dart';

// **************************************************************************
// JaguarSerializerGenerator
// **************************************************************************

abstract class _$ProductSerializer implements Serializer<Product> {
  @override
  Map<String, dynamic> toMap(Product model) {
    if (model == null) return null;
    Map<String, dynamic> ret = <String, dynamic>{};
    setMapValue(ret, 'comments', model.comments);
    setMapValue(ret, 'name', model.name);
    setMapValue(ret, 'brandName', model.brandName);
    setMapValue(ret, 'unit', model.unit);
    setMapValue(ret, 'price', passProcessor.serialize(model.price));
    setMapValue(ret, 'imageUrl', model.imageUrl);
    setMapValue(ret, 'taxStrategyID', model.taxStrategyID);
    setMapValue(ret, 'category', model.category);
    setMapValue(
        ret, 'discountedPrice', passProcessor.serialize(model.discountedPrice));
    setMapValue(ret, 'pkProdID', model.pkProdID);
    setMapValue(ret, 'barcode', model.barcode);
    setMapValue(ret, 'csvCategory', model.csvCategory);
    setMapValue(ret, 'serialNumber', model.serialNumber);
    return ret;
  }

  @override
  Product fromMap(Map map) {
    if (map == null) return null;
    final obj = new Product();
    obj.comments = map['comments'] as String;
    obj.name = map['name'] as String;
    obj.brandName = map['brandName'] as String;
    obj.unit = map['unit'] as String;
    obj.price = passProcessor.deserialize(map['price']);
    obj.imageUrl = map['imageUrl'] as String;
    obj.taxStrategyID = map['taxStrategyID'] as int;
    obj.category = map['category'] as String;
    obj.discountedPrice = passProcessor.deserialize(map['discountedPrice']);
    obj.pkProdID = map['pkProdID'] as int;
    obj.barcode = map['barcode'] as String;
    obj.csvCategory = map['csvCategory'] as String;
    obj.serialNumber = map['serialNumber'] as int;
    return obj;
  }
}
