import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

class _CameraOverlayCorners extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint();
    paint.color = Colors.black;
    paint.strokeWidth = 5;
    double width = size.width;
    double height = size.height;
    //TOP RIGHT
    canvas.drawLine(Offset(width + 2.5, 0), Offset(width - 30, 0), paint);
    canvas.drawLine(Offset(width, 0), Offset(width, 30), paint);
    //TOP LEFT
    canvas.drawLine(Offset(-2.5, 0), Offset(30, 0), paint);
    canvas.drawLine(Offset(0, 0), Offset(0, 30), paint);
    //BOTTOM RIGHT
    canvas.drawLine(
        Offset(width + 2.5, height), Offset(width - 30, height), paint);
    canvas.drawLine(Offset(width, height), Offset(width, height - 30), paint);
    //BOTTOM LEFT
    canvas.drawLine(Offset(-2.5, height), Offset(30, height), paint);
    canvas.drawLine(Offset(0, height), Offset(0, height - 30), paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

class CameraOverlayCutoutWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: () => SystemChannels.textInput.invokeMethod('TextInput.hide'),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              color: Colors.black.withOpacity(0.5),
              height: 0.32 * height,
              width: width,
            ),
            Row(
              children: <Widget>[
                Container(
                  color: Colors.black.withOpacity(0.5),
                  height: 0.33 * height,
                  width: 0.15 * width,
                ),
                Container(
                  width: 0.7 * width,
                  height: 0.33 * height,
                  child: CustomPaint(
                    size: Size(0.7 * width, 0.33 * height),
                    painter: _CameraOverlayCorners(),
                  ),
                ),
                Container(
                  color: Colors.black.withOpacity(0.5),
                  height: 0.33 * height,
                  width: 0.15 * width,
                ),
              ],
            ),
            Container(
              color: Colors.black.withOpacity(0.5),
              height: 0.35 * height,
              width: width,
            ),
          ],
        ),
      )
    );
  }
}
