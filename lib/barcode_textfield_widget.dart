import 'package:barcode_scanner/barcode_scanner_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class BarcodeTextFieldWidget extends StatefulWidget {
  final BarcodeScannerBloc barcodeScannerBloc;

  const BarcodeTextFieldWidget({Key key, this.barcodeScannerBloc})
      : super(key: key);

  @override
  _BarcodeTextFieldWidgetState createState() => _BarcodeTextFieldWidgetState();
}

class _BarcodeTextFieldWidgetState extends State<BarcodeTextFieldWidget> {
  final barcodeController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        contentPadding:
            EdgeInsets.only(top: 30, bottom: 30, left: 10, right: 10),
        title: StreamBuilder(
            stream: widget.barcodeScannerBloc.isSearchingForProduct,
            builder: (BuildContext context, AsyncSnapshot<bool> snapshot) {
              return TextField(
                decoration: InputDecoration(hintText: "Barcode number"),
                enabled: !(snapshot.data ?? false),
                keyboardType: TextInputType.number,
                controller: barcodeController,
              );
            }),
        trailing: RaisedButton(
          onPressed: () {
            this
                .widget
                .barcodeScannerBloc
                .barcodeSink
                .add(barcodeController.text);
          },
          child: Text('Find'),
        ),
      ),
    );
  }
}
