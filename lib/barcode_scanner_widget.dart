import 'package:barcode_scanner/barcode_scanner_camera.dart';
import 'package:barcode_scanner/data/entities/product.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class BarcodeScannerWidget extends StatelessWidget {
  final Function(Product, Exception) onProductFetched;

  BarcodeScannerWidget(this.onProductFetched);

  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(FontAwesomeIcons.barcode),
      onPressed: () async {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  BarcodeScannerCamera(onProductFetched: onProductFetched)),
        );
      },
    );
  }
}
