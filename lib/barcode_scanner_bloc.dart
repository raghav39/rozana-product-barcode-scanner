import 'dart:async';
import 'dart:typed_data';
import 'dart:ui';

import 'package:barcode_scanner/data/data_manager.dart';
import 'package:barcode_scanner/data/entities/product.dart';
import 'package:camera/camera.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/foundation.dart';

class BarcodeScannerBloc {
  final _dataManager = DataManager('asdf');

  final StreamController<Product> _productStreamController =
      StreamController<Product>();

  final StreamController<CameraImage> _imageStreamController =
      StreamController<CameraImage>();

  final StreamController<String> _barcodeStreamController =
      StreamController<String>();

  final StreamController<bool> _searchingForProduct =
      StreamController<bool>.broadcast();

  final int cameraOrientation;

  bool _isDetectingImage = false;

  BarcodeScannerBloc(this.cameraOrientation) {
    _searchingForProduct.add(false);

    _imageStreamController.stream.listen((CameraImage image) async {
      if (_isDetectingImage) {
        return;
      }
      _isDetectingImage = true;
      try {
        String barcode = await _scanCameraImageForBarcode(
          image,
        );
        if (barcode.isEmpty) {
          _isDetectingImage = false;
          return;
        }
        barcodeSink.add(barcode);
      } catch (e) {
        print(e);
        _isDetectingImage = false;
      }
    });

    _barcodeStreamController.stream.listen((String barcode) async {
      //once we get the image, we are not setting the _isDetectingImage as false because we do not need any new image
      await _getGeneralProductByBarcode(barcode);
    });
  }

  Future<String> _scanCameraImageForBarcode(CameraImage image) async {
    final _barcodeDetector = FirebaseVision.instance.barcodeDetector();
    ImageRotation rotation = _rotationIntToImageRotation(cameraOrientation);

    List<Barcode> barcodeList =
        await _barcodeDetector.detectInImage(FirebaseVisionImage.fromBytes(
      _concatenatePlanes(image.planes),
      _buildMetaData(image, rotation),
    ));

    String barcode;
    try {
      barcode = barcodeList[0]?.displayValue;
      print(barcode);
    } catch (e) {
      barcode = '';
    }
    return barcode;
  }

  Future<void> _getGeneralProductByBarcode(String barcode) async {
    _searchingForProduct.add(true);
    try {
      Product product = await _dataManager.getGeneralProductByBarcode(barcode);
      _searchingForProduct.add(false);
      _productStreamController.add(product);
    } catch (e) {
      print(e);
      _productStreamController.add(null);
      _searchingForProduct.add(false);
    }
  }

  Uint8List _concatenatePlanes(List<Plane> planes) {
    final WriteBuffer allBytes = WriteBuffer();
    planes.forEach((Plane plane) => allBytes.putUint8List(plane.bytes));
    return allBytes.done().buffer.asUint8List();
  }

  FirebaseVisionImageMetadata _buildMetaData(
    CameraImage image,
    ImageRotation rotation,
  ) {
    return FirebaseVisionImageMetadata(
      rawFormat: image.format.raw,
      size: Size(image.width.toDouble(), image.height.toDouble()),
      rotation: rotation,
      planeData: image.planes.map(
        (Plane plane) {
          return FirebaseVisionImagePlaneMetadata(
            bytesPerRow: plane.bytesPerRow,
            height: plane.height,
            width: plane.width,
          );
        },
      ).toList(),
    );
  }

  ImageRotation _rotationIntToImageRotation(int rotation) {
    switch (rotation) {
      case 0:
        return ImageRotation.rotation0;
      case 90:
        return ImageRotation.rotation90;
      case 180:
        return ImageRotation.rotation180;
      default:
        assert(rotation == 270);
        return ImageRotation.rotation270;
    }
  }

  Stream<Product> get productStream => _productStreamController.stream;

  Stream<bool> get isSearchingForProduct => _searchingForProduct.stream;

  Sink<CameraImage> get imageForBarcodeSink => _imageStreamController.sink;

  Sink<String> get barcodeSink => _barcodeStreamController.sink;

  void dispose() {
    _productStreamController.close();
    _imageStreamController.close();
    _searchingForProduct.close();
    _barcodeStreamController.close();
  }
}
