import 'package:barcode_scanner/barcode_scanner_bloc.dart';
import 'package:barcode_scanner/barcode_textfield_widget.dart';
import 'package:barcode_scanner/camera_overlay_cutout_widget.dart';
import 'package:barcode_scanner/data/entities/product.dart';
import 'package:barcode_scanner/data/exceptions/product_not_found.dart';
import 'package:camera/camera.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class BarcodeScannerCamera extends StatefulWidget {
  final Function(Product, Exception) onProductFetched;

  const BarcodeScannerCamera({Key key, this.onProductFetched})
      : super(key: key);

  @override
  _BarcodeScannerCameraState createState() => _BarcodeScannerCameraState();
}

class _BarcodeScannerCameraState extends State<BarcodeScannerCamera> {
  CameraController _camera;

  BarcodeScannerBloc _barcodeScannerBloc;

  @override
  void initState() {
    _initializeCamera();
    super.initState();
  }

  void _initializeCamera() async {
    CameraDescription description = await availableCameras().then(
      (List<CameraDescription> cameras) => cameras.firstWhere(
            (CameraDescription camera) =>
                camera.lensDirection == CameraLensDirection.back,
          ),
    );
    _barcodeScannerBloc = BarcodeScannerBloc(description.sensorOrientation);
    _listenForProduct();

    _camera = CameraController(
      description,
      defaultTargetPlatform == TargetPlatform.iOS
          ? ResolutionPreset.low
          : ResolutionPreset.medium,
    );
    await _camera.initialize();

    _camera.startImageStream((CameraImage image) async {
      if (mounted) {
        _barcodeScannerBloc.imageForBarcodeSink.add(image);
      }
    });
    setState(() {});
  }

  void _listenForProduct() {
    this._barcodeScannerBloc.productStream.listen((Product product) {
      _camera.stopImageStream();
      if (mounted) {
        Exception e;
        if(product == null){
          e = ProductNotFoundException();
        }
        this.widget.onProductFetched(product, e);
        Navigator.pop(context);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(),
        body: SafeArea(
          child: Container(
            constraints: const BoxConstraints.expand(),
            child: _camera == null
                ? const Center(
                    child: Text(
                      'Initializing Camera...',
                      style: TextStyle(
                        color: Colors.green,
                        fontSize: 30.0,
                      ),
                    ),
                  )
                : Stack(
                    children: <Widget>[
                      CameraPreview(_camera),
                      CameraOverlayCutoutWidget(),
                      BarcodeTextFieldWidget(
                          barcodeScannerBloc: _barcodeScannerBloc),
                      StreamBuilder(
                        stream: _barcodeScannerBloc.isSearchingForProduct,
                        builder: (BuildContext context,
                            AsyncSnapshot<bool> snapshot) {
                          if (snapshot.hasData && snapshot.data) {
                            return Center(
                              child: Container(
                                width: 50,
                                height: 50,
                                child: CircularProgressIndicator(),
                              ),
                            );
                          }
                          return Container();
                        },
                      )
                    ],
                  ),
          ),
        ));
  }

  @override
  void dispose() {
    _camera.dispose();
    _barcodeScannerBloc.dispose();
    super.dispose();
  }
}
